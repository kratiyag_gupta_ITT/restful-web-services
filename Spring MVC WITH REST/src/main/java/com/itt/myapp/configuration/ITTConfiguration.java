package com.itt.myapp.configuration;

import com.itt.myapp.controller.HomeController;
import com.itt.myapp.dao.EmployeeDao;
import com.itt.myapp.dao.EmployeeDaoImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;
import com.itt.myapp.service.EmployeeService;
import com.itt.myapp.service.EmployeeServiceImpl;
import com.itt.myapp.service.ValidatorService;
import java.util.Properties;
import javax.sql.DataSource;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
@EnableWebMvc
@ComponentScan(basePackages = "com.itt.myapp")
@EnableTransactionManagement
public class ITTConfiguration extends WebMvcConfigurerAdapter
  {

    @Autowired
    private SessionFactory sessionFactory;

    @Bean(name = "HelloWorld")
    public ViewResolver viewResolver()
      {
        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        viewResolver.setViewClass(JstlView.class);
        viewResolver.setPrefix("/WEB-INF/views/");
        viewResolver.setSuffix(".jsp");
        return viewResolver;
      }

    @Bean(name = "employeeService")
    public EmployeeService employeeService()
      {
        return new EmployeeServiceImpl();
      }
    /**
     * Configure the database to dataSource object using programatic approach
     * @return 
     */
    @Bean
    public DataSource dataSource()
      {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("com.mysql.jdbc.Driver");
        dataSource.setUrl("jdbc:mysql://localhost:3306/organization");
        dataSource.setUsername("root");
        dataSource.setPassword("kratiyag");
        return dataSource;
      }
  
    @Bean
    public HibernateTransactionManager transactionManager()
      {
        HibernateTransactionManager hibernateTransactionManager = new HibernateTransactionManager();
        hibernateTransactionManager.setSessionFactory(sessionFactory);
        return hibernateTransactionManager;
      }

    @Bean
    public HibernateTemplate hibernateTemplate()
      {
        HibernateTemplate hibernateTemplate = new HibernateTemplate();
        hibernateTemplate.setSessionFactory(sessionFactory);
        return hibernateTemplate;
      }

    @Bean
    public LocalSessionFactoryBean sessionFactory()
      {
        LocalSessionFactoryBean lsfb = new LocalSessionFactoryBean();
        lsfb.setDataSource(dataSource());
        lsfb.setHibernateProperties(getHibernateProperties());
        lsfb.setPackagesToScan(new String[]
          {
            "com.itt.myapp.domain"
          });
        return lsfb;
      }

    /**
     * Set Hibernate Configuration information using Coding Technique
     * @return Objects Holding the properties value
     */
    @Bean
    public Properties getHibernateProperties()
      {
        Properties properties = new Properties();
        properties.put("hibernate.dialect", "org.hibernate.dialect.MySQLDialect");
        properties.put("hibernate.show_sql", "true");
        properties.put("hibernate.hbm2ddl.auto", "update");
        properties.put("hibernate.generate_statistics", "true");
        properties.put("hibernate.id.new_generator_mappings", "false");
        return properties;
      }

    @Bean(name = "employeeDao")
    public EmployeeDao employeeDao()
      {
        return new EmployeeDaoImpl();
      }

    @Bean(name = "validatorService")
    public ValidatorService validatorService()
      {
        return new ValidatorService();
      }

    /**
     * For determining to load resource folder to server so that HTML resources
     * can be added
     *
     * @param registry
     */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry)
      {
        registry.addResourceHandler("/resource/**").addResourceLocations("/resource/");
      }
  }
