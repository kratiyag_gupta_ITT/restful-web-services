/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itt.myapp.dao;

import com.itt.myapp.domain.Employee;
import com.itt.myapp.domain.Login;
import java.util.List;

/**
 *
 * @author kratiyag.gupta
 */
public interface EmployeeDao
	{
		void register(Employee employee);
		Employee validateUser(Login login);
		Employee validateUser(String email);
		public List<Employee> getUsersData();
                public void deleteEmployee(int EmployeeId);
                public void update(Employee employee);
	}
