/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itt.myapp.dao;

import com.itt.myapp.controller.HomeController;
import com.itt.myapp.domain.Employee;
import com.itt.myapp.domain.Login;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import javax.sql.DataSource;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author kratiyag.gupta
 */
@Repository
@Qualifier("facultyDao")
@Transactional
public class EmployeeDaoImpl implements EmployeeDao
  {
    private static final Logger logger = LoggerFactory
            .getLogger(HomeController.class);

    @Autowired
    HibernateTemplate hibernateTemplate;

    @Override
    public void register(Employee employee)
      {
          if(validateUser(employee.getEmail())!=null)
          {
          }
          else
          {
              hibernateTemplate.save(employee);
          }
        
      }

    @Override
    public Employee validateUser(Login login)
      {
          
        DetachedCriteria cr = DetachedCriteria.forClass(Employee.class);
        cr.add(Restrictions.eq("email", login.getUsername())).add(Restrictions.eq("password", login.getPassword()));
        List<Employee> employee = (List<Employee>) hibernateTemplate.findByCriteria(cr);
        logger.info("employee detail"+employee.size());
        return (Employee) (employee.size() > 0 ? employee.get(0) : null);
      }

    /**
     *
     * @param email Id of user
     * @return
     */
    @Override
    public Employee validateUser(String email)
      {
        DetachedCriteria cr = DetachedCriteria.forClass(Employee.class);
        cr.add(Restrictions.eq("email", email));
        List<Employee> employee = (List<Employee>) hibernateTemplate.findByCriteria(cr);
        return (Employee) (employee.size() > 0 ? employee.get(0) : null);
      }

    @Override
    public List<Employee> getUsersData()
      {
        List<Employee> employee = hibernateTemplate.loadAll(Employee.class);
        return employee;
      }

    @Override
    public void deleteEmployee(int EmployeeId)
    {
        Employee emp=new Employee();
        emp.setEmployeeId(EmployeeId);
        hibernateTemplate.delete(emp);
    }

    @Override
    public void update(Employee employee) 
    {
      hibernateTemplate.update(employee);
    }
  }

/**
 * Custom Row mapper for displaying limited values to user
 *
 * @author kratiyag.gupta
 */
class EmployeeMapper implements RowMapper<Employee>
  {

    @Override
    public Employee mapRow(ResultSet rs, int arg1) throws SQLException
      {
        Employee employee = new Employee();
        employee.setEmployeeId(rs.getInt("employee_id"));
        employee.setFirstName(rs.getString("firstName"));
        employee.setLastName(rs.getString("lastName"));
        employee.setPassword(rs.getString("password"));
        employee.setDateOfBirth(rs.getString("date_of_Birth"));
        employee.setJoiningDate(rs.getString("joining_date"));
        employee.setGender(rs.getString("gender"));
        employee.setEmail(rs.getString("email"));
        return employee;
      }
  }
