/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itt.myapp.controller;

import com.itt.myapp.domain.Employee;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 *
 * @author kratiyag.gupta
 */
public class EmployeeValidator implements Validator
{
    
     @Override
     public boolean supports(Class clazz) 
     {
        return Employee.class.isAssignableFrom(clazz);
     }

     @Override
    public void validate(Object target, Errors errors) 
    {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "firstName", errors
                .getFieldError().getCode(), "First name is required.");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "lastName", errors
                .getFieldError().getCode(),
                "Last name is required.");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", errors
                .getFieldError().getCode(),
                "Email is required.");

    }
}
