package com.itt.myapp.controller;

import com.itt.myapp.domain.Employee;
import com.itt.myapp.service.EmployeeService;
import java.util.List;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author kratiyag.gupta
 */
@RestController
public class RestServicesr 
{
    @Autowired
    EmployeeService employeeService;
    /**
     * For Getting the data of Employees from database 
     * @return List of all Employees
     */
    @RequestMapping(value = {"/employeedata"},method = RequestMethod.GET)
    public List<Employee> getData()
    {
       return employeeService.getUsersData();
    }
    /**
     * For deleting the Employee with specified employee Id
     * @param employeeId     Holds Employee Id of employee whose data is to be deleted               
     * @return 
     */
    @RequestMapping(value = {"/delete/{id}"},method = RequestMethod.DELETE)
    public Response deleteEmployee(@PathVariable("id") int employeeId)
    {
        employeeService.deleteEmployee(employeeId);
        return Response.status(Response.Status.OK).build();
    }
    /**
     * Update the data 
     * @param employee
     * @param result
     * @return 
     */
    @RequestMapping(value = {"/update"},method = RequestMethod.PUT)
    public Response update(@Validated @RequestBody Employee employee,BindingResult result)
    {
        if(result.hasErrors())
        {
            return Response.status(Response.Status.BAD_REQUEST).entity(result.getAllErrors()).build();
        }
        employeeService.update(employee);
        return Response.status(Response.Status.OK).entity(employee).build();
    }
    
    @RequestMapping(value={"/create"},method = RequestMethod.POST,consumes =MediaType.APPLICATION_FORM_URLENCODED )
    public Response create(@Validated @ModelAttribute("employee") Employee employee,BindingResult result)
    {
        Response response=customValidation(employee);
        if(response!=null)
        {
            return response;
        }
        if(result.hasErrors())
        {
            return Response.status(Response.Status.BAD_REQUEST).entity(result.getAllErrors()).build();
        }
        employeeService.register(employee);
        return Response.status(Response.Status.OK).entity(employee).build();
    }
    
    public Response customValidation(Employee employee)
    {
        if(!isValidPassword(employee.getPassword(), employee.getConfirmPassword()))
        {
            return Response.status(Response.Status.BAD_REQUEST).encoding("password and Confirm password not match").build();
        }
        return null;
                
    }
    public boolean isValidPassword(String password,String ConfirmPassword)
    {
        return password.equals(ConfirmPassword);
    }
}
