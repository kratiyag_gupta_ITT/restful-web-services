/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itt.myapp.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import org.springframework.validation.FieldError;

/**
 *
 * @author kratiyag.gupta
 */
class ValidationErrorDTO implements Serializable
{
     private int status;
     private String message;
     private List<FieldError> fieldErrors = new ArrayList<FieldError>();

    public ValidationErrorDTO() {
    }

     
    public ValidationErrorDTO(int status,String message) 
    {
        this.status=status;
        this.message=message;
    }
    
      public int getStatus() {
            return status;
        }

        public String getMessage() {
            return message;
        }

        public void addFieldError(String path, String message,String defaultMessage) {
            FieldError error = new FieldError(path, message,defaultMessage);
            fieldErrors.add(error);
        }

        public List<FieldError> getFieldErrors() {
            return fieldErrors;
        }
}
