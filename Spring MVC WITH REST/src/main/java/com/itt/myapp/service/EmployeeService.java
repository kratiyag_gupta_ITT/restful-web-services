/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itt.myapp.service;

import com.itt.myapp.domain.*;
import java.util.List;

public interface EmployeeService 
{
    void register(Employee employee);
    Employee validateUser(Login login);
    Employee validateUser(String email);
    public List<Employee> getUsersData();
    public void deleteEmployee(int employeeId);
    public void update(Employee employee);
}
