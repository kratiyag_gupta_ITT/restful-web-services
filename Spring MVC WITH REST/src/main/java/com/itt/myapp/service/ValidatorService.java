/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itt.myapp.service;

import com.itt.myapp.controller.HomeController;
import java.util.Calendar;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author kratiyag.gupta
 */
public class ValidatorService
  {

    private static final Logger logger = LoggerFactory
            .getLogger(HomeController.class);

    /**
     *
     * @param name holds the name of Employee
     * @return boolean value is value is true or false
     */
    public boolean isValidName(String name)
      {

        if (name.matches("[0-9]"))
          {
            return false;
          }
        return true;
      }

    /**
     *
     * @param email Holds the email Id of the user
     * @return	Boolean value after checking the email validity
     */
    public boolean isValidEmail(String email)
      {
        if (!email.isEmpty() && email.matches(
                "[a-zA-Z0-9.!#$%&�*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\\.[a-zA-Z0-9-]+)*$"))
          {
            return true;
          }
        logger.info("email is not valid");
        return false;
      }

    /**
     *
     * For Determining the validity of Date of Birth of Employee
     */

    public boolean isValidBirthDate(String birthDate)
      {

        if (birthDate.contains("-"))
          {
            logger.info("Current yerar: "
                    + Calendar.getInstance().get(Calendar.YEAR));
            String a[] = birthDate.split("-");
            logger.info(a[0] + " " + a[1] + " " + a[2]);
            int day = Integer.parseInt(a[2]);
            int month = Integer.parseInt(a[1]);
            int currentYear = Calendar.getInstance()
                    .get(Calendar.YEAR);
            if ((Integer.parseInt(a[0]) <= currentYear - 15)
                    && day > 0 && day <= 31 && month > 0
                    && month <= 31)
              {
                return true;
              }
            else
              {
                return false;
              }
          }
        else
          {
            return false;
          }
      }

    /**
     * Compare both the password and return boolean result if match then true
     * else false
     *
     * @param password	Holds the value of password of user
     * @param confirmPassword	Holds the value of Confirm password of user
     * @return
     */
    public boolean isvalidPawword(String password, String confirmPassword)
      {
        if (!password.isEmpty() && !confirmPassword.isEmpty()
                && (confirmPassword.equals(password))
                && password.length() >= 8)
          {
            return true;
          }
        return false;
      }

    public boolean isValidEmployeeId(String str)
      {
        logger.info("inside validEmployee");
        if (str.matches("[a-z]"))
          {
              logger.info("return false");
            return false;
          }
        else
          {
              logger.info("return true");
            return true;
          }
      }
  }
