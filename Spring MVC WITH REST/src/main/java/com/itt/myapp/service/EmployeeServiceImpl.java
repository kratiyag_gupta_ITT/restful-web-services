/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itt.myapp.service;

import com.itt.myapp.dao.EmployeeDao;
import com.itt.myapp.domain.Employee;
import org.springframework.stereotype.Service;
import com.itt.myapp.domain.Login;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

@Service("EmployeeService")
public class EmployeeServiceImpl implements EmployeeService
  {

    @Autowired
    EmployeeDao employeeDao;

    @Override
    public void register(Employee employee)
      {
        employeeDao.register(employee);
      }

    @Override
    public Employee validateUser(Login login)
      {
        return employeeDao.validateUser(login);
      }

    @Override
    public Employee validateUser(String email)
      {
        return employeeDao.validateUser(email);
      }

    @Override
    public List<Employee> getUsersData()
    {
        return employeeDao.getUsersData();
    }

    @Override
    public void deleteEmployee(int employeeId)
    {
       employeeDao.deleteEmployee(employeeId);
    }

    @Override
    public void update(Employee employee) 
    {
        employeeDao.update(employee);
    }

  }
