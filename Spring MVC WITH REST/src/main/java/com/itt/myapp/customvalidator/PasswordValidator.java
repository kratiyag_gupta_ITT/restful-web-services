/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itt.myapp.customvalidator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 *
 * @author kratiyag.gupta
 */
public class PasswordValidator implements ConstraintValidator<PasswordConstraint, String>{

    @Override
    public void initialize(PasswordConstraint a) 
    {
        
    }

    @Override
    public boolean isValid(String password, ConstraintValidatorContext cvc) 
    {
        return (password.matches("(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}"));
    }
    
}
