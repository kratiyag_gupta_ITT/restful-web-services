/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itt.hibernatespring.service;

import com.itt.hibernatespring.bean.Employee;
import com.itt.hibernatespring.dao.EmployeeDao;
import java.util.List;
import java.util.Scanner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author kratiyag.gupta
 */
@Service("EmployeeService")
@Transactional
public class EmployeeServiceImpl implements EmployeeService
  {

    @Autowired
    EmployeeDao employeeDao;
    Scanner in;
    Employee employee;

    /**
     * Default Constructor for Initialization
     */
    public EmployeeServiceImpl()
      {
        in = new Scanner(System.in);
      }

    /**
     * Calls for DAO layer for invoking save method
     */
    @Override
    public void save()
      {
       // employee = setEmployeeData();
        employee=new Employee("2017-06-03", "kratiyag", "gupta", "M", "2015-03-06", "kratiyag", "kratiyag");
         employeeDao.save(employee);
      }

    /**
     * Calls for DAO layer for invoking delete method
     */

    @Override
    public void delete()
      {
        employee = getEmployeeId();
        employeeDao.delete(employee);
      }
    /**
     * Calls for DAO layer for invoking update method
     */
    @Override
    public void update()
      {
        employee = getUpdateData();
        employeeDao.update(employee);
      }
    /**
     * Set the data entered by the user to Employee Persistence class
     * @return Employee object Holding the users Entered data
     */
    @Override
    public Employee setEmployeeData()
      {
        Employee emp = new Employee();
        System.out.println("Enter Employee First Name");
        emp.setFirstName(in.next());
        System.out.println("Enter Employee Last Name");
        emp.setLastName(in.next());
        System.out.println("Enter Employee joining date");
        emp.setJoiningDate(in.next());
        System.out.println("Enter Employee Date of  Birth");
        emp.setDateOfBirth(in.next());
        System.out.println("Enter email id");
        emp.setEmail(in.next());
        System.out.println("Enter password");
        emp.setPassword(in.next());
        System.out.println("Enter Gender");
        emp.setGender(in.next().charAt(0) + "");
        return emp;
      }
    
    /**
     * Holds the Employee Id
     * @return Object Containing the Employee Id
     */
    @Override
    public Employee getEmployeeId()
      {
        Employee emp = new Employee();
        System.out.println("Enter Employee Id");
        emp.setEmployeeId(in.nextInt());
        return emp;
      }
    /**
     * Stores the fields with the updated value
     * @return Employee object containing the values of updated fields
     */
    @Override
    public Employee getUpdateData()
      {
        Employee emp;
        int choice;
        emp = employeeDao.getEmployeeData();
        System.out.println("Select the field to update");
        System.out.println("1.For changing first name");
        System.out.println("2For changiing last name");
        System.out.println("3.For changing password");
        choice = in.nextInt();
        switch (choice)
          {
            case 1:
                System.out.println("Enter new First Name");
                emp.setFirstName(in.next());
                break;
            case 2:
                System.out.println("Enter new Last Name");
                emp.setLastName(in.next());
                break;
            case 3:
                System.out.println("Enter New Password");
                emp.setPassword(in.next());
                break;
            default:
                System.out.println("Invalid Choice");
                break;
          }
        return emp;
      }
    /**
     * Getting and printing Employee Data
     */
    @Override
    public Employee getEmployeeData()
      {
        Employee emp = employeeDao.getEmployeeData();
        emp.printData();
        return emp;
      }

    @Override
    public List<Employee> getEmployeeDataAll() 
    {
        List<Employee> emp=employeeDao.getEmployeeDataAll();
        for (Employee emp1 : emp) 
        {
            emp1.printData();
        }
        return emp;
    }

    private void Employee(String string, String kratiyag, String gupta, String m, String string0, String kratiyag0, String kratiyag1) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
  }
