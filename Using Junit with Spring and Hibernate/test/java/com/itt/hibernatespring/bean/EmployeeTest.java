/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itt.hibernatespring.bean;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;


/**
 *
 * @author kratiyag.gupta
 */
public class EmployeeTest 
{
    Employee instance;
    public EmployeeTest() 
    {
        instance = new Employee();
    }
    
    @BeforeClass
    public static void setUpClass()
    {    
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }



    /**
     * Test of getDateOfBirth method, of class Employee.
     */
    @Test
    public void testGetDateOfBirth() 
    {
        System.out.println("getDateOfBirth");
        instance.setDateOfBirth("2017-05-03");
        String result = instance.getDateOfBirth();
        assertNotNull(result);
    }

    /**
     * Test of setDateOfBirth method, of class Employee.
     */
    @Test
    public void testSetDateOfBirth() 
    {
        System.out.println("setDateOfBirth");
        String dateOfBirth = "2017-05-03";
        try{
        instance.setDateOfBirth(dateOfBirth);
        }
        catch(Exception e)
        {
            fail("failed");
        }
    }

    /**
     * Test of getFirstName method, of class Employee.
     */
    @Test
    public void testGetFirstName() {
        System.out.println("getFirstName");
        instance.setFirstName("kratiyag");
        String result = instance.getFirstName();
        assertNotNull(result);
    }

    /**
     * Test of setFirstName method, of class Employee.
     */
    @Test
    public void testSetFirstName() {
        System.out.println("setFirstName");
        String firstName = "kratiyag";
        instance.setFirstName(firstName);
        assertEquals(instance.getFirstName(), firstName);
    }

    /**
     * Test of getLastName method, of class Employee.
     */
    @Test
    public void testGetLastName() 
    {
        System.out.println("getLastName");
        instance.setLastName("gupta");
        String result = instance.getLastName();
        assertEquals("gupta", result);
    }

    /**
     * Test of setLastName method, of class Employee.
     */
    @Test
    public void testSetLastName() 
    {
        System.out.println("setLastName");
        String lastName = "gupta";
        Employee instance = new Employee();
        instance.setLastName(lastName);
        assertEquals(lastName, instance.getLastName());
    }

    /**
     * Test of getGender method, of class Employee.
     */
    @Ignore
    public void testGetGender() {
        System.out.println("getGender");
        Employee instance = new Employee();
        String expResult = "";
        String result = instance.getGender();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setGender method, of class Employee.
     */
    @Ignore
    public void testSetGender() {
        System.out.println("setGender");
        String gender = "";
        Employee instance = new Employee();
        instance.setGender(gender);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getJoiningDate method, of class Employee.
     */
    @Ignore
    public void testGetJoiningDate() {
        System.out.println("getJoiningDate");
        Employee instance = new Employee();
        String expResult = "";
        String result = instance.getJoiningDate();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setJoiningDate method, of class Employee.
     */
    @Ignore
    public void testSetJoiningDate() {
        System.out.println("setJoiningDate");
        String joiningDate = "";
        Employee instance = new Employee();
        instance.setJoiningDate(joiningDate);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getPassword method, of class Employee.
     */
    @Ignore
    public void testGetPassword() {
        System.out.println("getPassword");
        Employee instance = new Employee();
        String expResult = "";
        String result = instance.getPassword();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setPassword method, of class Employee.
     */
    @Ignore
    public void testSetPassword() {
        System.out.println("setPassword");
        String password = "";
        Employee instance = new Employee();
        instance.setPassword(password);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getEmail method, of class Employee.
     */
    @Ignore
    public void testGetEmail() {
        System.out.println("getEmail");
        Employee instance = new Employee();
        String expResult = "";
        String result = instance.getEmail();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setEmail method, of class Employee.
     */
    @Ignore
    public void testSetEmail() {
        System.out.println("setEmail");
        String email = "";
        Employee instance = new Employee();
        instance.setEmail(email);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of printData method, of class Employee.
     */
    @Ignore
    public void testPrintData() {
        System.out.println("printData");
        Employee instance = new Employee();
        instance.printData();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }
    
}
