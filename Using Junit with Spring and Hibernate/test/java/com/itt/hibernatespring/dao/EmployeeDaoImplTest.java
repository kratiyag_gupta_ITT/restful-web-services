/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itt.hibernatespring.dao;

import com.itt.hibernatespring.bean.Employee;
import com.itt.hibernatespring.configuration.SpringConfiguration;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.rules.Timeout;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author kratiyag.gupta
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {SpringConfiguration.class})
@Transactional
public class EmployeeDaoImplTest 
{
    @Autowired
    EmployeeDao employeeDao;
    
    @Rule
    public Timeout timeout = new Timeout(2000);
    
    public EmployeeDaoImplTest() {
    }
    
    /**
     * Test of save method, of class EmployeeDaoImpl.
     */
    @Test
    public void testSave() throws InterruptedException 
    {
        System.out.println("save");
        Employee employee = new Employee("2012-03-25", "Kratiyag", "Gupta", "M", "2012-03-06", "kratiyag", "kratiyag");
        Thread.sleep(1005);
        employeeDao.save(employee);
        List <Employee> empList=employeeDao.getEmployeeDataAll();
        Employee emp=empList.get(empList.size()-1);
        assertSame(employee.getEmail(), emp.getEmail());
    }

    /**
     * Test of update method, of class EmployeeDaoImpl.
     */
    @Test
    @Rollback(true)
    public void testUpdate() 
    {
        System.out.println("Enter Employee Id");
        Employee employee = new Employee("2012-03-25", "Naresh", "Gupta", "M", "2012-03-06", "kratiyag", "kratiyag");
        employee.setEmployeeId(29);
        employeeDao.update(employee);
        List <Employee> empList=employeeDao.getEmployeeDataAll();
        for (Employee empList1 : empList)
        {
            if(empList1.getEmployeeId()==30)
            {
                assertSame("Upadation Failed updated data is not found in any changes in database",employee, empList1);
                break;
            }
        }
    }

    /**
     * Test of delete method, of class EmployeeDaoImpl.
     */
    @Test
    @Rollback(true)
    public void testDelete() 
    {
        System.out.println("delete");
        Employee employee = new Employee();
        employee.setEmployeeId(24);
        employeeDao.delete(employee);
        List<Employee> emp=employeeDao.getEmployeeDataAll();
        for (Employee emp1 : emp)
        {
            if(emp1.getEmployeeId()==24)
            {
                fail();
                break;
            }
        }
    }

    /**
     * Test of getEmployeeData method, of class EmployeeDaoImpl.
     */
    @Ignore
    @Rollback(true)
    public void testGetEmployeeData() 
    {
        System.out.println("getEmployeeData");
        EmployeeDaoImpl instance = new EmployeeDaoImpl();
        Employee expResult = null;
        Employee result = instance.getEmployeeData();
        assertEquals(expResult, result);
    }

    /**
     * Test of getEmployeeDataAll method, of class EmployeeDaoImpl.
     */
    @Test
    @Rollback(true)
    public void testGetEmployeeDataAll() 
    {
        System.out.println("getEmployeeDataAll");
        List<Employee> expResult = employeeDao.getEmployeeDataAll();
        List<Employee> result = employeeDao.getEmployeeDataAll();
        assertEquals(expResult, result);
    }
    
}
