package com.itt.hibernatespring.Test;

import com.itt.hibernatespring.bean.Employee;
import com.itt.hibernatespring.dao.EmployeeDaoImpl;
import org.springframework.transaction.annotation.Transactional;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author kratiyag.gupta
 */

@org.junit.runner.RunWith(org.junit.runners.Suite.class)
@org.junit.runners.Suite.SuiteClasses({EmployeeDaoImpl.class,Employee.class})
@Transactional
public class AllTest 
{
    
}
