
package com.itt.restfulweb.dao;

import com.itt.restfulweb.beans.Employee;
import com.itt.restfulweb.config.HibernateUtil;
import java.io.FileInputStream;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.cfg.Configuration;

/**
 * @author kratiyag.gupta
 */
public class EmployeeDaoImpl implements EmployeeDao
{
    FileInputStream fis;
    Properties props;
    Configuration configuration; 
    Scanner in;

    /**
     * Get Employee Data from Employee Database
     * @return 
     */
    @Override
    public List<Employee> getEmployeeData() 
    {
        Session  session=HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        Criteria cr=session.createCriteria(Employee.class); 
        List<Employee> list=cr.list();
        session.getTransaction().commit();
        return list;
    }
    /**
     * Get Data of Employee with specific employee Id
     * @param employeeId
     * @return 
     */
    @Override
    public Employee getEmployeeDataById(int employeeId)
    {
       Session session=HibernateUtil.getSessionFactory().getCurrentSession();
       session.beginTransaction();
       Employee employee=(Employee)session.get(Employee.class, employeeId);
      session.getTransaction().commit();
      return employee; 
    }
    /**
     * Add data of Employee to the Database
     * @param employee 
     */
    @Override
    public void addEmployee(Employee employee)
    {
        Session session=HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        session.save(employee);
        session.getTransaction().commit();
    }
    /**
     * Delete Data of Employee 
     * @param employeeId  Id of Employee Whose Data is to be deleted
     */
    @Override
    public void deleteEmployee(int employeeId) 
    {
        Session session=HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        Employee emp=new Employee();
        emp.setEmployeeId(employeeId);
        session.delete(emp);
        session.getTransaction().commit();
    }
/**
 * Update Data of Employee according to Data 
 * @param employee 
 */
    @Override
    public void updateEmployeeData(Employee employee) 
    {
        Session session=HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        session.update(employee);
        session.getTransaction().commit();
    }
    
}
