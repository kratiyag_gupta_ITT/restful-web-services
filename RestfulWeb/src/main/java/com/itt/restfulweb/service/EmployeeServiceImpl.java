/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itt.restfulweb.service;

import com.itt.restfulweb.beans.Employee;
import com.itt.restfulweb.dao.EmployeeDao;
import com.itt.restfulweb.dao.EmployeeDaoImpl;
import java.util.List;

/**
 * @author kratiyag.gupta
 */
public class EmployeeServiceImpl implements EmployeeService
{

    /**
     * Calls for Dao getEmployeeData()
     * @return List of Employee from Database
     */    
    @Override
    public List<Employee> getEmployeeData()
    {
        EmployeeDao empDao=new EmployeeDaoImpl();
        return empDao.getEmployeeData();
    }
    /**
     * Calls for Dao getEmployeeDataById(int employeeId)
     * @param employeeId
     * @return Employee object
     */
    @Override
    public Employee getEmployeeDataById(int employeeId) 
    {
      EmployeeDao empDao=new EmployeeDaoImpl();
      return empDao.getEmployeeDataById(employeeId);
    }
    /**
     * Calls for DAO addEmployee(Employee employee)
     * @param employee Holds Employee data
     */
    @Override
    public void addEmployee(Employee employee) 
    {
        EmployeeDao empDao=new EmployeeDaoImpl();
        empDao.addEmployee(employee);
    }
    /**
     * Calls for deleteEmployee(employeeId)
     * @param employeeId 
     */
    @Override
    public void deleteEmployee(int employeeId)
    {
        EmployeeDao empDao=new EmployeeDaoImpl();
        empDao.deleteEmployee(employeeId);
    }
    /**
     * Calls For DAO updateEmployeeData(Employee employee) 
     * @param employee 
     */
    @Override
    public void updateEmployeeData(Employee employee) 
    {
        EmployeeDao empDao=new EmployeeDaoImpl();
        empDao.updateEmployeeData(employee);
    }
    
}
