/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itt.restfulweb.service;

import com.itt.restfulweb.beans.Employee;
import java.util.List;

/**
 *
 * @author kratiyag.gupta
 */
public interface EmployeeService 
{
    public List<Employee> getEmployeeData();
    public Employee getEmployeeDataById(int employeeId);
    public void addEmployee(Employee employee);
    public void deleteEmployee(int employeeId);
    public void updateEmployeeData(Employee employee);
}
