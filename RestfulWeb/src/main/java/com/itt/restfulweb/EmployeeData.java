package com.itt.restfulweb;

import com.itt.restfulweb.beans.Employee;
import com.itt.restfulweb.service.EmployeeService;
import com.itt.restfulweb.service.EmployeeServiceImpl;
import java.io.IOException;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;


/**
 * @author kratiyag.gupta
 */
@Path("/employeedata")
public class EmployeeData 
{

    @Context
    private UriInfo context;


    /**
     *Function to get All the Employees
     * @return an instance of java.util.List Holding employee Data
     */
    @GET
    @Path("users")
    @Produces("application/json")
    public List<Employee> getAllEmployee() 
    {
        EmployeeService employeeService = new EmployeeServiceImpl();

        return employeeService.getEmployeeData();
    }
    /**
     * Get User Data From Database by Id of Employee
     * @param employeeId
     * @return Employee Object
     */
    @GET
    @Path("usersdata/{userId}")
    @Produces("application/json")
    public Employee getuserData(@PathParam("userId") int employeeId) 
    {
        EmployeeService employeeService = new EmployeeServiceImpl();
        return employeeService.getEmployeeDataById(employeeId);
    }
    /**
     * Stores data of Employee in the Database 
     */
    @POST
    @Path("/create")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public void addEmployee(@FormParam("first-name") String firstName, @FormParam("last-name") String lastName,
            @FormParam("password") String password, @FormParam("confirm-password") String confirmPassword,
            @FormParam("date-of-birth") String dateOfBirth, @FormParam("joining-date") String joiningDate, @FormParam("email") String email,
            @FormParam("gender-list") String gender, @Context HttpServletResponse response) throws IOException 
    {

        Employee emp = new Employee();
        emp.setDateOfBirth(dateOfBirth);
        emp.setEmail(email);
        emp.setFirstName(firstName);
        emp.setLastName(lastName);
        emp.setGender(gender);
        emp.setJoiningDate(joiningDate);
        emp.setPassword(password);
        emp.setConfirmPassword("hello");
        EmployeeService empService = new EmployeeServiceImpl();
        empService.addEmployee(emp);
        response.sendRedirect("./users/");
    }

    /**
     * @param id Holds Id of Employee
     * @param headers  Holds the object of header receive from HTML request
     * @return
     */
    @DELETE
    @Path("/delete/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_XML)
    public String deleteEmployee(@PathParam("id") int id, @Context HttpHeaders headers) 
    {
        String userName = headers.getRequestHeader("username").get(0);
        String userPassword = headers.getRequestHeader("password").get(0);
        if (userName.equals("intimetec") && userPassword.equals("intimetec"))
        {
            EmployeeService empService = new EmployeeServiceImpl();
            empService.deleteEmployee(id);
            return "<html><body>Employee Information Deleted with Id " + id + "</body></html>";
        }
        else
        {
            return "<html><body>Invalid UserName and Password</body></html>";
        }
    }

    /**
     * Update Employee Data
     * @param emp           Holds the Employee object
     * @return an HTTP response with content of the updated or created resource.
     */
    @PUT
    @Path("/update")
    @Consumes("application/json")
    @Produces(MediaType.TEXT_XML)
    public String updateEmployee(Employee emp,@Context HttpHeaders headers) 
    {
        String userName = headers.getRequestHeader("username").get(0);
        String userPassword = headers.getRequestHeader("password").get(0);
        if(userName.equals("intimetec") && userPassword.equals("intimetec"))
        {
            EmployeeService employeeService = new EmployeeServiceImpl();
            employeeService.updateEmployeeData(emp);
            return "<html><body>Employee Information Information Updated with Id " + emp.getEmployeeId() + "</body></html>";
        }
        else
        {
            return "<html><body>Invalid UserName And Password</body></html>";
        }
    }
}
